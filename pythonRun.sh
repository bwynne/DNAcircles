# Name environment
ENV_NAME=DNAcircles
ACTIVATE=${PWD}/${ENV_NAME}/bin/activate
KERNEL_PATH=~/.local/share/jupyter/kernels/

# Install virtualenv if you need
#pip3 install virtualenv

# Install packages if this is fresh start
if [[ ! -e "${ACTIVATE}" ]]; then
    python3 -m virtualenv ${ENV_NAME}
    source ${ACTIVATE}
    pip3 install numpy scipy matplotlib tensorflow keras

    # This part adds the venv to Jupyter
    if [[ ! -e "${KERNEL_PATH}/${ENV_NAME}" ]]; then
        pip3 install ipykernel
        python3 -m ipykernel install --user --name=${ENV_NAME}
    else
        echo "Kernel named ${ENV_NAME} already exists in ${KERNEL_PATH}"
    fi
else
    source ${ACTIVATE}
fi

